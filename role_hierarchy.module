<?php

/**
 * @file
 * The role_hierarchy module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Access\AccessResult;
use Drupal\user\Plugin\Action\AddRoleUser;
use Drupal\user\Plugin\Action\RemoveRoleUser;
use Drupal\role_hierarchy\Plugin\Action\RoleHierarchyAddRoleUser;
use Drupal\role_hierarchy\Plugin\Action\RoleHierarchyRemoveRoleUser;

/**
 * Helper function used to remove roles that shouldn't be accessible on forms.
 */
function _role_hierarchy_alter_form_roles(&$form, FormStateInterface $form_state) {
  // Users cannot add or remove user roles on a level higher than their role.
  $current_user = \Drupal::currentUser()->getAccount();

  if ($current_user->hasPermission('bypass role hierarchy')) {
    return;
  }

  /** @var \Drupal\user\Entity\User $editer_user */
  $editer_user = $form_state->getFormObject()->getEntity();
  /** @var \Drupal\role_hierarchy\Service\RoleHierarchyHelper $role_hierarchy_helper */
  $role_hierarchy_helper = \Drupal::service('role_hierarchy.helper');

  $form['account']['roles']['#access'] = $form['account']['roles']['#access']
    || $current_user->hasPermission('edit user roles');

  foreach ($form['account']['roles']['#options'] as $role => &$label) {
    // When editing your own account, never hide roles that you already have.
    if ($editer_user->id() == $current_user->id() && in_array($role, $current_user->getRoles())) {
      continue;
    }

    if (!$role_hierarchy_helper->hasRoleEditAccess($current_user, $role)) {
      unset($form['account']['roles']['#options'][$role]);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for user_form.
 */
function role_hierarchy_form_user_form_alter(array &$form, FormStateInterface $form_state) {
  _role_hierarchy_alter_form_roles($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter() for user_register_form.
 */
function role_hierarchy_form_user_register_form_alter(array &$form, FormStateInterface $form_state) {
  _role_hierarchy_alter_form_roles($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter() for user_admin_roles_form.
 */
function role_hierarchy_form_user_admin_roles_form_alter(array &$form, FormStateInterface $form_state) {
  /** @var \Drupal\Core\Config\ImmutableConfig $config */
  $config = \Drupal::configFactory()->getEditable('role_hierarchy.settings');
  $messenger = \Drupal::messenger();

  $messenger->addWarning(t('Role hierarchy is enabled. Users will only be able to add or edit users having roles that appear <b>@direction</b> their role in this list.', [
    '@direction' => !empty($config->get('invert')) ? 'above' : 'below',
  ]));

  if (empty($config->get('strict'))) {
    $messenger->addWarning(t('Users can edit other users with equal roles.'));
  }

  $form['role_hierarchy'] = [
    '#type' => 'details',
    '#title' => t('Role hierarchy'),
    '#open' => TRUE,
  ];

  $form['role_hierarchy']['invert'] = [
    '#type' => 'checkbox',
    '#default_value' => $config->get('invert'),
    '#title' => t('Invert hierarchy'),
    '#description' => t('By inverting the hierarchy, roles will only be able to edit the other roles <b>above</b> them instead of below. Reordering will be done automatically when the form is submitted.'),
  ];

  $form['role_hierarchy']['strict'] = [
    '#type' => 'checkbox',
    '#default_value' => $config->get('strict'),
    '#title' => t('Strict hierarchy check'),
    '#description' => t('If this is checked, roles will no longer be able to edit equal roles.'),
  ];

  $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
  $role_options = [];
  foreach ($roles as $role) {
    $role_options[$role->id()] = $role->label();
  }

  $form['role_hierarchy']['non_hierarchical_roles'] = [
    '#type' => 'checkboxes',
    '#default_value' => $config->get('non_hierarchical_roles') ?? [],
    '#title' => t('Non hierarchy roles'),
    '#description' => t("Roles selected here are not part of the hierarchy and will have no impact on the account's edit permissions"),
    '#options' => $role_options,
  ];

  array_unshift($form['#submit'], '_role_hierarchy_user_admin_roles_form_submit');
}

/**
 * Submit callback for user_admin_roles_form form.
 *
 * This callback is used to save the module settings.
 */
function _role_hierarchy_user_admin_roles_form_submit(array $form, FormStateInterface $form_state) {
  $config = \Drupal::configFactory()->getEditable('role_hierarchy.settings');

  $old_invert = $config->get('invert');
  $new_invert = $form_state->getValue('invert');

  if ($old_invert != $new_invert) {
    $config->set('invert', $new_invert);

    $entities = $form_state->getValue('entities');
    foreach ($entities as &$entity) {
      $entity['weight'] = -$entity['weight'];
    }

    $form_state->setValue('entities', $entities);
  }

  $strict = $form_state->getValue('strict');
  $config->set('strict', $strict);

  $non_hierarchical_roles = $form_state->getValue('non_hierarchical_roles');
  $config->set('non_hierarchical_roles', $non_hierarchical_roles);

  $config->save();
}

/**
 * Implements hook_ENTITY_TYPE_access() for entity type "user".
 */
function role_hierarchy_user_access(User $entity, $operation, $account) {
  if (!in_array($operation, ['update', 'delete'])) {
    return AccessResult::neutral();
  }

  /** @var \Drupal\role_hierarchy\Service\RoleHierarchyHelper $role_hierarchy_helper */
  $role_hierarchy_helper = \Drupal::service('role_hierarchy.helper');
  return AccessResult::forbiddenIf(!$role_hierarchy_helper->hasEditAccess($account, $entity));
}

/**
 * Implements hook_action_info_alter().
 */
function role_hierarchy_action_info_alter(&$definitions) {
  foreach ($definitions as &$definition) {
    if ($definition['id'] === 'user_add_role_action' && $definition['class'] == AddRoleUser::class) {
      $definition['class'] = RoleHierarchyAddRoleUser::class;
    }
    if ($definition['id'] === 'user_remove_role_action' && $definition['class'] == RemoveRoleUser::class) {
      $definition['class'] = RoleHierarchyRemoveRoleUser::class;
    }
  }
}
