<?php

namespace Drupal\role_hierarchy\Service;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Class RoleHierarchyHelper provides core functions for role_hierarchy.
 */
class RoleHierarchyHelper {

  /**
   * The module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected $roleStorage;

  /**
   * RoleHierarchyHelper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entity_type_manager) {
    $this->settings = $configFactory->get('role_hierarchy.settings');
    $this->roleStorage = $entity_type_manager->getStorage('user_role');
  }

  /**
   * Get the lowest/highest user role, depending on settings.
   *
   * @param \Drupal\user\UserInterface $user
   *   The current user.
   *
   * @return int
   *   The user  role weight.
   */
  public function getUserRoleWeight(UserInterface $user) {
    return $this->getAccountRoleWeight($user);
  }

  /**
   * Get the lowest/highest account role, depending on settings.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account.
   *
   * @return int
   *   The account role weight.
   */
  public function getAccountRoleWeight(AccountInterface $account) {
    $highest_role = $this->getAccountHighestRole($account);

    if (empty($highest_role)) {
      return !empty($this->settings->get('invert'))
        ? -9999
        : 9999;
    }

    return $highest_role->getWeight();
  }

  /**
   * Get the lowest/highest account role, depending on settings.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account.
   *
   * @return \Drupal\user\RoleInterface
   *   The account role.
   */
  public function getAccountHighestRole(AccountInterface $account) {
    $account_roles = $this->roleStorage->loadMultiple(array_values($account->getRoles()));
    $non_hierarchical_roles = $this->settings->get('non_hierarchical_roles') ?? [];
    $account_roles = array_filter($account_roles, function ($role) use ($non_hierarchical_roles) {
      return empty($non_hierarchical_roles[$role->id()]);
    });

    if (empty($account_roles)) {
      return NULL;
    }

    $sorted_account_roles = [];
    foreach ($account_roles as $account_role) {
      $sorted_account_roles[$account_role->id()] = $this->getRoleWeight($account_role);
    }
    asort($sorted_account_roles);

    if (!empty($this->settings->get('invert'))) {
      return $this->roleStorage->load(array_key_last($sorted_account_roles));
    }

    return $this->roleStorage->load(array_key_first($sorted_account_roles));
  }

  /**
   * Get the weight of a role as configured in /admin/people/roles.
   *
   * @param Role|string $role
   *   The user role.
   *
   * @return int
   *   The weight of the role.
   */
  public function getRoleWeight($role) {
    if ($role == 'workflow_author') {
      return 9999;
    }

    if (is_string($role)) {
      $role = $this->roleStorage->load($role);
    }

    return $role->getWeight();
  }

  /**
   * Check if an user can edit an user role by comparing their weights.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user editing the role.
   * @param \Drupal\user\RoleInterface|string $edited_role
   *   The edited user role.
   *
   * @return bool
   *   True if the account can edit the role.
   */
  public function hasRoleEditAccess(AccountInterface $account, $edited_role) {
    if ($account->hasPermission('bypass role hierarchy')) {
      return TRUE;
    }

    $user_role_weight = $this->getAccountRoleWeight($account);
    $edited_role_weight = $this->getRoleWeight($edited_role);

    if (!empty($this->settings->get('invert'))) {
      if (!empty($this->settings->get('strict'))) {
        return $user_role_weight > $edited_role_weight;
      }
      else {
        return $user_role_weight >= $edited_role_weight;
      }
    }
    else {
      if (!empty($this->settings->get('strict'))) {
        return $user_role_weight < $edited_role_weight;
      }
      else {
        return $user_role_weight <= $edited_role_weight;
      }
    }
  }

  /**
   * Determines if an account has edit access on an user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account that tries to edit.
   * @param \Drupal\user\Entity\User $user
   *   The user.
   *
   * @return bool
   *   True if the account can edit the user.
   */
  public function hasEditAccess(AccountInterface $account, User $user) {
    if ($account->id() == 1) {
      return TRUE;
    }

    if ($account->hasPermission('bypass role hierarchy')) {
      return TRUE;
    }

    if ($account->id() == $user->id()) {
      return TRUE;
    }

    if ($user->id() == 1) {
      return FALSE;
    }

    return $this->hasRoleEditAccess($account, $this->getAccountHighestRole($user));
  }

  /**
   * Checks object access.
   *
   * Helper function for RoleHierarchyAddRoleUser/RoleHierarchyRemoveRoleUser.
   *
   * @param mixed $object
   *   The object to execute the action on.
   * @param string $role
   *   The added/removed role.
   * @param bool|\Drupal\Core\Access\AccessResultInterface $parent_return
   *   The return of the parent plugin.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The user for which to check access, or NULL to check access
   *   for the current user. Defaults to NULL.
   * @param bool $return_as_object
   *   (optional) Defaults to FALSE.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The access result. Returns a boolean if $return_as_object is FALSE (this
   *   is the default) and otherwise an AccessResultInterface object.
   *   When a boolean is returned, the result of AccessInterface::isAllowed() is
   *   returned, i.e. TRUE means access is explicitly allowed, FALSE means
   *   access is either explicitly forbidden or "no opinion".
   *
   * @see \Drupal\role_hierarchy\Plugin\Action\RoleHierarchyRemoveRoleUser::access()
   * @see \Drupal\role_hierarchy\Plugin\Action\RoleHierarchyAddRoleUser::access()
   *
   * @see \Drupal\Core\Action\ActionInterface::access()
   */
  public function actionPluginAccess($object, $role, $parent_return, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // Adding a null check for $account
    if ($account === NULL) {
      return $return_as_object ? AccessResult::forbidden() : FALSE;
    }

    if (in_array($role, $account->getRoles())) {
      return $parent_return;
    }

    if (!$this->hasRoleEditAccess($account, $role)) {
      return $return_as_object ? AccessResult::forbidden() : FALSE;
    }

    return $parent_return;
  }

}
