<?php

namespace Drupal\role_hierarchy\Plugin\Action;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\role_hierarchy\Service\RoleHierarchyHelper;
use Drupal\user\Plugin\Action\AddRoleUser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a role to a user.
 *
 * @Action(
 *   id = "user_add_role_action",
 *   label = @Translation("Add a role to the selected users"),
 *   type = "user"
 * )
 */
class RoleHierarchyAddRoleUser extends AddRoleUser {

  /**
   * The role hierarchy helper.
   *
   * @var \Drupal\role_hierarchy\Service\RoleHierarchyHelper
   */
  protected $roleHierarchyHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeInterface $entity_type, RoleHierarchyHelper $role_hierarchy_helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type);
    $this->roleHierarchyHelper = $role_hierarchy_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getDefinition('user_role'),
      $container->get('role_hierarchy.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $parent_return = parent::access($object, $account, TRUE);
    $role = $this->configuration['rid'];
    return $this->roleHierarchyHelper->actionPluginAccess($object, $role, $parent_return, $account, $return_as_object);
  }

}
